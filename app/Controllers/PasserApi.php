<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;
use App\Models\OAuthModel;

class PasserApi extends ResourceController
{
    protected $modelName = 'App\Models\PassModel';
    protected $format = 'json';
    protected $oauth;

    public function __construct()
    {
        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }

    public function passer() //Отображение всех записей
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $data = $this->model->getPasswords($OAuthModel->getUser()->user_id == 'administrator' ? null : $OAuthModel->getUser()->id, $search, $per_page);
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['passwords' => $data, 'pager' => $model->pager->getDetails('group1')]);
        } else $this->oauth->server->getResponse()->send();
    }

}