<?php namespace App\Controllers;

use App\Models\PassModel;
use Aws\S3\S3Client;

class Pass extends BaseController
{
    public function index() //Отображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PassModel();
        $data ['passer'] = $model->getUser();
        $data['id_user'] = $this-> ionAuth->user()->row()->id;
        echo view('data/view_all', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('data/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'sitename' => 'required|min_length[3]|max_length[255]',
                'data_login'  => 'required',
                'data_password'  => 'required',
                'date'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new PassModel();
            $data = [
                'sitename' => $this->request->getPost('sitename'),
                'data_login' => $this->request->getPost('data_login'),
                'data_password' => $this->request->getPost('data_password'),
                'date' => $this->request->getPost('date'),
                'id_user' => $this->ionAuth->user()->row()->id,
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Запись создана'));
            return redirect()->to('/pass');
        }
        else
        {
            return redirect()->to('/pass/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PassModel();

        helper(['form']);
        $data ['passer'] = $model->getUser($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('data/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form','url']);
        echo '/pass/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'sitename' => 'required|min_length[3]|max_length[255]',
                'data_login'  => 'required',
                'data_password'  => 'required',
                'date'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new PassModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'sitename' => $this->request->getPost('sitename'),
                'data_login' => $this->request->getPost('data_login'),
                'data_password' => $this->request->getPost('data_password'),
                'date' => $this->request->getPost('date'),
                'id_user' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);

            return redirect()->to('/pass');
        }
        else
        {
            return redirect()->to('/pass/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PassModel();
        $model->delete($id);
        return redirect()->to('/pass');
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PassModel();
        $data ['passer'] = $model->getUser($id);
        echo view('data/view', $this->withIon($data));
    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new PassModel();
            $data['passer'] = $model->getPassWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('data/view_all_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Требуются права администратора'));
            return redirect()->to('/auth/login');
        }
    }
}