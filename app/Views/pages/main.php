<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">
    <img class="mb-4" src="https://image.flaticon.com/icons/png/512/159/159478.png" alt="" width="72" height="72"><h1 class="display-4">MyPass</h1>
    <p class="lead">Это приложение поможет хранить входные данные о всех ваших сайтах в одном месте.</p>
    <?php if (! $ionAuth->loggedIn()): ?>
    <a class="btn btn-primary btn-lg" href="<?=base_url()?>/auth/login" role="button">Войти</a>
    <?php else: ?>
        <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/pass" role="button">Перейти к паролям</a></p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
