<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Данные всех пользователей:</h2>
        <div class="d-flex mb-2">
            <?= $pager->links('group1','my_page') ?>
            <?= form_open('pass/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('pass/viewAllWithUsers',['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
            </form>
        </div>

    </div>
    <table class="table table-striped">
    <thead>
    <th scope="col">Название сайта</th>
    <th scope="col">Логин</th>
    <th scope="col">Пароль</th>
    <th scope="col">Дата создания записи</th>

    </thead>
<?php if (!empty($passer) && is_array($passer)) : ?>
    <tbody>
    <?php foreach ($passer as $item): ?>
        <tr>
            <td><?= esc($item['sitename']); ?></td>
            <td><?= esc($item['data_login']); ?></td>
            <td><?= esc($item['data_password']); ?></td>
            <td><?= esc($item['date']); ?></td>
            <td>
                <a href="<?= base_url()?>/pass/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                <a href="<?= base_url()?>/pass/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/pass/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>

<?php else : ?>
    </tbody>
    </table>
    <div class="text-center">
        <p>Записи не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/pass/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать запись</a>
    </div>
<?php endif ?>
    </div>
<?= $this->endSection() ?>