<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($passer)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <?php if (!is_null($passer['picture_url'])) : ?>
                            <img src="<?= $passer['picture_url'] ?>" class="card-img-top p-3" alt="oops" />
                        <?php else:?>
                            <img src="https://image.flaticon.com/icons/png/512/1231/1231223.png" class="card-img-top p-3" alt="oops" />
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <p class="card-text">Сайт: <?= esc($passer['sitename']); ?></p>
                            <p class="card-text">Логин: <?= esc($passer['data_login']); ?></p>
                            <p class="card-text">Пароль: <?= esc($passer['data_password']); ?></p>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Пароль актуален на:</div>
                                <div class="text-muted"><?= esc(Time::parse(esc($passer['date']))->toDateString() ); ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Пароль не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>