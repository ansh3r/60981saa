<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">

        <h2 align="center">Все пароли</h2>
        <div class="d-flex justify-content-center">
            <?php if ($ionAuth->user()->row()->id == 1) : ?>
                <a class="btn btn-success" href="<?=base_url()?>/pass/viewallwithusers" role="button">Просмотр паролей всех пользователей</a>
            <?php endif ?>
        </div>
        <?php if (!empty($passer) && is_array($passer)) : ?>
            <?php foreach ($passer as $item): ?>
            <?php if ($item['id_user'] == $id_user) : ?>

                <div class="card mb-3" style="max-width:600px; margin: 0 auto;">
                    <div class="row d-flex align-items-center">

                        <div class="col-md-3">
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img class="card-img-top" height="75" src="https://image.flaticon.com/icons/png/512/1231/1231223.png" alt="Card image cap" width="30" height="50">
                            <?php else:?>
                                <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['sitename']); ?>">
                            <?php endif ?>


                        </div>

                        <div class="col-md-3 d-flex justify-content-center">
                            <div style="word-break: break-all;"><?= esc($item['sitename'])?></div>
                        </div>
                        <div class="col-md-6">
                            <div style="margin: 10px 0px;">
<!--                                <h5 class="card-title">--><?//= esc($item['data_password']); ?><!--</h5>-->
<!--                                <p class="card-text">--><?//= esc($item['sitename']); ?><!--</p>-->
                                <?php if ($item['id_user'] == $id_user) : ?>
                                <a href="<?= base_url()?>/index.php/pass/view/<?= esc($item['id']); ?>" class="btn btn-sm btn-primary">Просмотреть</a>
                                <a href="<?= base_url()?>/index.php/pass/edit/<?= esc($item['id']); ?>" class="btn btn-sm btn-success">Изменить</a>
                                <a href="<?= base_url()?>/index.php/pass/delete/<?= esc($item['id']); ?>" class="btn btn-sm btn-danger">Удалить</a>
                                <?php endif ?>

                            </div>
                        </div>

                    </div>

                </div>


                <?php endif ?>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти записи.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>