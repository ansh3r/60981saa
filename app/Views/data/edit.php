<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('pass/update'); ?>
        <input type="hidden" name="id" value="<?= $passer["id"] ?>">

        <div class="form-group">
            <label for="sitename">Название сайта</label>
            <input type="text" class="form-control <?= ($validation->hasError('sitename')) ? 'is-invalid' : ''; ?>" name="sitename"
                   value="<?= $passer["sitename"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('sitename') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="data_login">Логин</label>
            <input type="text" class="form-control <?= ($validation->hasError('data_login')) ? 'is-invalid' : ''; ?>" name="data_login"
                   value="<?= $passer["data_login"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('data_login') ?>
            </div>

        </div>
        <div class="form-group">
        <label for="data_password">Пароль</label>
        <input type="password" class="form-control <?= ($validation->hasError('data_password')) ? 'is-invalid' : ''; ?>" name="data_password"
               value="<?= $passer["data_password"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('data_password') ?>
        </div>
        <div class="form-group">
            <label for="date">Актуальная дата</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>" name="date" value="<?= $passer["date"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
            <div class="form-group">
                <label for="birthday">Изображение</label>
                <input type="file" accept=".jpg, .jpeg, .png, .bmp, .tif, .gif" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
                <div class="invalid-feedback">
                    <?= $validation->getError('picture') ?>
                </div>
            </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>