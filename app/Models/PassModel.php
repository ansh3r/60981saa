<?php namespace App\Models;
use CodeIgniter\Model;
class PassModel extends Model
{
    protected $table = 'passer'; //таблица, связанная с моделью
    protected $allowedFields = ['sitename', 'data_login', 'data_password', 'date', 'id_user', 'picture_url'];
    public function getUser($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
    public function getPassWithUser($id = null, $search = '')
    {
        $builder = $this->select('*, passer.id')->join('users','passer.id_user = users.id')->like('sitename', $search,'both', null, true)->orlike('sitename',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['passer.id' => $id])->first();
        }
        return $builder;
    }

    // Если $user_id == null то возвращаются все рейтинги, иначе - только принадлежащие пользователю с user_id
    public function getPasswords($user_id = null, $search = '', $per_page = null)
    {
        $model = $this->like('sitename', is_null($search) ? '' : $search, 'both');
        if (!is_null($user_id)) {
            $model = $model->where('user_id', $user_id);
        }
        // Пагинация
        return $model->paginate($per_page, 'group1');
    }
}