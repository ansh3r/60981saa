<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public $server;

    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $storage = new MyPdo(array('dsn' => 'pgsql:host=ec2-34-247-151-118.eu-west-1.compute.amazonaws.com;port=5432;dbname=dae12rabm9raa0;user=glzqjfdgaowiky;password=25a50cd6f03bfe83e8f0ce005dcee9e815d189203e92531d1d42b718b2ec640a'),array('user_table' => 'users'));
        $grantType = new UserCredentials($storage);
        $this->server = new Server($storage);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}