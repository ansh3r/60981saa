<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('185917589178-p5vbrjksboa4nc7kli1jvm68rd8ev67r.apps.googleusercontent.com');
        $this->google_client->setClientSecret('BpjAH--VTdO3SDILWPaaG5CQ');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }
}