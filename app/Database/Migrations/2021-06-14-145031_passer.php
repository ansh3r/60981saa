<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class passer extends Migration
{

    public function up()
    {
        // groups
        if (!$this->db->tableexists('passer'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'data_login' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'data_password' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'sitename' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'date' => array('type' => 'date', 'null' => FALSE),
                'id_user' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));
            // create table
            $this->forge->addForeignKey('id_user', 'users', 'id', 'RESTRICT', 'RESTRICT');
            $this->forge->createtable('passer', TRUE);

        }
        // users
        // users_groups
    }
    //--------------------------------------------------------------------
    public function down()
    {
        $this->forge->droptable('passer');
    }
}